package com.bricks.alert.controller;

import com.bricks.alert.domain.Brick;
import com.bricks.alert.domain.Order;
import com.bricks.alert.service.OrderService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderControllerTest {
	@Mock
	OrderService orderService;
	@InjectMocks
	OrderController orderController = new OrderController();

	@Test
	public void contextLoads() throws Exception {
		orderController.getBricks(123);
		Assert.assertEquals("abc",1,1);
	}


}
