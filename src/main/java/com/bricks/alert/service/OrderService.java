package com.bricks.alert.service;

import com.bricks.alert.domain.Brick;
import com.bricks.alert.domain.Order;

import java.util.List;

public interface OrderService {

    Integer createOrder(Order order);
    List<Brick> getBricks(Integer order) throws Exception;

    String updateOrder(Order order) throws Exception;
}
