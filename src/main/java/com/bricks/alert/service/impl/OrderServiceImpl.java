package com.bricks.alert.service.impl;

import com.bricks.alert.domain.Brick;
import com.bricks.alert.domain.Order;
import com.bricks.alert.service.OrderService;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
@Service
public class OrderServiceImpl implements OrderService {

    private List<Brick> bricks = new ArrayList<>();
    private Map<Integer, Order> orders = new HashMap<>();
    private static Random r = new Random();
    @Override
    public Integer createOrder(Order order) {
        int orderno = getRandomNumberInRange();
        orders.put(orderno,order);
        order.setId(orderno);
        order.setStatus("INITIATED");
        bricks.addAll(createBricks(order.getQuantity(),order.getId()));

        return order.getId();
    }
    public synchronized List<Brick> getBricks(Integer orderId) throws Exception {
        Order order1 = orders.get(orderId);
        List<Brick> bricksList = new ArrayList<>();
        if(order1!=null) {
            order1.setStatus("DISPATCHED");
            bricksList = getBricksOf(orderId);
            if (bricksList.isEmpty()) {
                throw new Exception("Internal Server Error");
            }
            bricks.removeAll(bricksList);
            order1.setStatus("DELIVERED");
        }
        return bricksList;

    }
    private List<Brick> getBricksOf(Integer orderId){
        return bricks.stream().filter(o->o.getOrderId().equals(orderId)).collect(Collectors.toList());
    }

    @Override
    public synchronized String updateOrder(Order order) throws Exception {
        Order order1= orders.get(order.getId());
        if(order1!=null && order1.getStatus().equals("INITIATED")) {
            long count = getBricksOf(order.getId()).size();
            if (order.getQuantity() < count) {
                bricks.removeAll(getBricks(count - order.getQuantity(), order.getId()));
            } else{
                bricks.addAll(getBricks(order.getQuantity(), order1.getId()));
            }
        }else {
            throw new Exception("Invalid order or items already delivered");
        }

        return "Update Successfully";
    }

    private List<Brick> createBricks(int quantity,Integer orderId){
        int i=1;
        List<Brick> bricks = new ArrayList<>();
        while (i<=quantity){
            bricks.add(new Brick(i++,orderId));
        }
        return bricks;
    }
    private List<Brick> getBricks(long quantity,Integer orderId){
        int i=1;
        List<Brick> bricks = new ArrayList<>();
        for (Brick brick: bricks){
            if(i<=quantity)
                bricks.add(brick);
        }
        return bricks;
    }
    private static int getRandomNumberInRange() {
        return r.nextInt((249845 - 1) + 1) + 1;
    }
}
