package com.bricks.alert.domain;

public class Brick {
    private int brickId;
    private Integer orderId;

    public Brick(int brickId, Integer orderId) {
        this.brickId = brickId;
        this.orderId = orderId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public int getBrickId() {
        return brickId;
    }

    public void setBrickId(int brickId) {
        this.brickId = brickId;
    }
}
