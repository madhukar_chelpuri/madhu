package com.bricks.alert.controller;

import com.bricks.alert.domain.Brick;
import com.bricks.alert.domain.Order;
import com.bricks.alert.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@EnableSwagger2
public class OrderController {
	@Autowired
	OrderService orderService;

	@PostMapping("order/create")
	public Integer createOrder(@RequestBody Order order) {
		return orderService.createOrder(order);
	}
	@PutMapping("/order/update")
	public String updateOrder(@RequestBody Order order) throws Exception {
		return orderService.updateOrder(order);
	}

	@RequestMapping(value = "/bricks/{orderId}", method = RequestMethod.GET)
	public List<Brick> getBricks(@PathVariable("orderId") Integer orderId) throws Exception {
		return orderService.getBricks(orderId);
	}
	@ExceptionHandler(value = Exception.class)
	public ResponseEntity<Object> exception(Exception exception) {
		return new ResponseEntity<>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
